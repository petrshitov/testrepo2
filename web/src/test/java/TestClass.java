import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class TestClass extends DriverInit {

    @DataProvider(name = "yandexMarket")
    public Object[][] yandexMarket() {
        return new Object[][]{
                {"Case 1 - check smartphones", "Мобильные телефоны", "Производитель Samsung", "40000", ""},
                {"Case 2 - check headphones", "Наушники и Bluetooth-гарнитуры", "Производитель Beats", "17000", "25000"},
        };
    }


    @Test(dataProvider = "yandexMarket")
    public void testOne(String log, String section, String producer, String priceFrom, String priceTo) throws InterruptedException {
        System.out.println(log);
        driver.get("http://yandex.ru");
        driver.findElement(By.xpath("//*[@data-id = 'market']")).click();
        driver.findElement(By.xpath("//*[contains(text(), 'Электроника')]")).click();
        invisibilityOfElementLocated(By.xpath("(//a[text() = 'Мобильные телефоны'])[2]"));
        driver.findElement(By.xpath("(//a[text() = '" + section + "'])[2]")).click();
        presenceOfElementLocated(By.name(producer));
        actions.moveToElement(driver.findElement(By.name(producer))).click().build().perform();
        driver.findElement(By.id("glpricefrom")).sendKeys(priceFrom);
        driver.findElement(By.id("glpriceto")).sendKeys(priceTo);
        Thread.sleep(2000);
        String titleOnTheList = driver.findElement(By.xpath("(//*[@class = 'n-snippet-cell2__title'])[1]")).getText();
        driver.findElement(By.xpath("(//*[@class = 'n-snippet-cell2__title'])[1]")).click();

        Assert.assertEquals(titleOnTheList, driver.findElement((By.xpath("//*[@class = 'title title_size_28 title_bold_yes']"))).getText());
    }

    @Test
    public void testTwo() throws InterruptedException, IOException {
        System.out.println("Case 3 - Find Alfa-bank career opportunities and write it's to file");
        driver.get("http://yandex.ru");
        driver.findElement(By.id("text")).sendKeys("Альфа-Банк");
        driver.findElement(By.xpath("//*[@class = 'search2__button']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("(//*[@class='needsclick'])[1]")).click();

        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(0));
        driver.close();
        driver.switchTo().window(tabs2.get(1));
        driver.findElement(By.linkText("Вакансии")).click();
        driver.findElement(By.xpath("(//*[@class = 'nav_item-link-helper'])[5]")).click();
        String text = driver.findElement(By.xpath("//*[@class='info']")).getText();

        String date = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName();

        FileWriter fileWriter = new FileWriter("d:\\" + date + "_" + browserName + "_yandex" + ".txt");
        fileWriter.write(text);
        fileWriter.close();

    }
}
